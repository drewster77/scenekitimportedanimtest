//Maya ASCII 2018ff07 scene
//Name: cube-blend.ma
//Last modified: Fri, Apr 27, 2018 01:02:10 PM
//Codeset: UTF-8
requires maya "2018ff07";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201711281015-8e846c9074";
fileInfo "osv" "Mac OS X 10.13.4";
createNode transform -s -n "persp";
	rename -uid "7842BC28-DD4E-0520-29B6-BEBAF9A13BEC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.4868742633109582 2.0403286632943498 6.2410664448752895 ;
	setAttr ".r" -type "double3" -17.641667234032209 13.400369304116023 4.08696344745255e-16 ;
	setAttr ".rp" -type "double3" -1.1102230246251565e-16 -2.1792463666958639e-17 0 ;
	setAttr ".rpt" -type "double3" -2.2816147490574332e-16 5.1593117237915555e-16 1.2412533556384829e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "AD47513C-0D40-C8A9-C6BC-EC9248DF30EB";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 6.7323581677227873;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.2116696535002802e-13 -9.1066043594878465e-14 6.7501559897209518e-14 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "D44CEBA0-274B-0691-ACA0-EE9340E101A4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "C236DA4C-774A-5D75-E36C-F1B902440D9A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "6E711206-2E46-A147-F95C-67BBE57ED7D6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "4DA494FD-6C47-13EF-C489-E482CA726AE5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "96C9EFA8-ED4B-E57B-9813-048EF4C82412";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "4BA9358E-EC4C-6EE3-E3E8-E2B54D56CEC7";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube2";
	rename -uid "3F946002-4143-5630-39DD-82987DC8B270";
	setAttr -av ".v";
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "CB82065E-AD45-AC0E-15CB-7A9EBEF183EE";
	setAttr -k off ".v";
	setAttr -s 14 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "pCubeShape2Orig" -p "pCube2";
	rename -uid "E9013CB3-5E49-8C45-256C-E3B4E7F87E18";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 98 ".cp";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 146 ".uvst[0].uvsp[0:145]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0.125 0.5 0 0.5 1 0.625 0.125 0.5 0.25 0.375 0.125
		 0.5 0.375 0.625 0.375 0.75 0.25 0.5 0.5 0.25 0.25 0.375 0.375 0.5 0.625 0.625 0.625
		 0.875 0.125 0.5 0.75 0.125 0.125 0.375 0.625 0.5 0.875 0.625 0.875 0.75 0 0.5 1 0.25
		 0 0.375 0.875 0.75 0.125 0.75 0 0.875 0.125 0.75 0.25 0.25 0.125 0.25 0 0.25 0.25
		 0.125 0.125 0.4375 0.0625 0.375 0.0625 0.4375 0 0.4375 1 0.5 0.0625 0.4375 0.125
		 0.4375 0.3125 0.3125 0.25 0.375 0.3125 0.4375 0.25 0.5 0.3125 0.4375 0.375 0.4375
		 0.5625 0.125 0.1875 0.375 0.5625 0.4375 0.5 0.5 0.5625 0.4375 0.625 0.4375 0.8125
		 0.1875 0 0.375 0.8125 0.4375 0.75 0.5 0.8125 0.4375 0.875 0.6875 0.0625 0.625 0.0625
		 0.625 0.9375 0.6875 0 0.75 0.0625 0.6875 0.125 0.1875 0.0625 0.125 0.0625 0.375 0.6875
		 0.1875 0 0.25 0.0625 0.1875 0.125 0.5625 0.0625 0.5625 0 0.5625 1 0.5625 0.125 0.5625
		 0.1875 0.625 0.1875 0.5625 0.25 0.5 0.1875 0.4375 0.1875 0.375 0.1875 0.5625 0.3125
		 0.625 0.3125 0.6875 0.25 0.5625 0.375 0.5625 0.4375 0.625 0.4375 0.8125 0.25 0.5625
		 0.5 0.5 0.4375 0.4375 0.4375 0.1875 0.25 0.375 0.4375 0.5625 0.5625 0.625 0.5625
		 0.875 0.1875 0.5625 0.625 0.5625 0.6875 0.625 0.6875 0.875 0.0625 0.5625 0.75 0.5
		 0.6875 0.4375 0.6875 0.375 0.6875 0.5625 0.8125 0.625 0.8125 0.8125 0 0.5625 0.875
		 0.5625 0.9375 0.625 0.9375 0.5625 1 0.5 0.9375 0.4375 0.9375 0.4375 1 0.3125 0 0.375
		 0.9375 0.8125 0.0625 0.8125 0 0.875 0.0625 0.8125 0.125 0.8125 0.1875 0.875 0.1875
		 0.8125 0.25 0.75 0.1875 0.6875 0.1875 0.6875 0.25 0.3125 0.0625 0.3125 0 0.3125 0.125
		 0.3125 0.1875 0.3125 0.25 0.25 0.1875 0.1875 0.1875 0.1875 0.25 0.125 0.1875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 98 ".vt[0:97]"  -1 -1 1 1 -1 1 -1 1 1 1 1 1 -1 1 -1 1 1 -1
		 -1 -1 -1 1 -1 -1 0 0 1 0 -1 1 1 0 1 0 1 1 -1 0 1 0 1 0 1 1 0 0 1 -1 -1 1 0 0 0 -1
		 1 0 -1 0 -1 -1 -1 0 -1 0 -1 0 1 -1 0 -1 -1 0 1 0 0 -1 0 0 -0.5 -0.5 1 -1 -0.5 1 -0.5 -1 1
		 0 -0.5 1 -0.5 0 1 -0.5 1 0.5 -1 1 0.5 -0.5 1 1 0 1 0.5 -0.5 1 0 -0.5 0.5 -1 -1 0.5 -1
		 -0.5 1 -1 0 0.5 -1 -0.5 0 -1 -0.5 -1 -0.5 -1 -1 -0.5 -0.5 -1 -1 0 -1 -0.5 -0.5 -1 0
		 1 -0.5 0.5 1 -0.5 1 1 -1 0.5 1 -0.5 0 1 0 0.5 -1 -0.5 -0.5 -1 -0.5 -1 -1 -0.5 0 -1 0 -0.5
		 0.5 -0.5 1 0.5 -1 1 0.5 0 1 0.5 0.5 1 1 0.5 1 0.5 1 1 0 0.5 1 -0.5 0.5 1 -1 0.5 1
		 0.5 1 0.5 1 1 0.5 0.5 1 0 0.5 1 -0.5 1 1 -0.5 0.5 1 -1 0 1 -0.5 -0.5 1 -0.5 -1 1 -0.5
		 0.5 0.5 -1 1 0.5 -1 0.5 0 -1 0.5 -0.5 -1 1 -0.5 -1 0.5 -1 -1 0 -0.5 -1 -0.5 -0.5 -1
		 0.5 -1 -0.5 1 -1 -0.5 0.5 -1 0 0.5 -1 0.5 0 -1 0.5 -0.5 -1 0.5 -1 -1 0.5 1 -0.5 -0.5
		 1 0 -0.5 1 0.5 -0.5 1 0.5 0 1 0.5 0.5 -1 -0.5 0.5 -1 0 0.5 -1 0.5 0.5 -1 0.5 0 -1 0.5 -0.5;
	setAttr -s 192 ".ed";
	setAttr ".ed[0:165]"  0 28 0 2 33 0 4 38 0 6 43 0 0 27 0 1 47 0 2 32 0 3 65 0
		 4 37 0 5 74 0 6 42 0 7 82 0 9 56 0 10 59 0 11 60 0 12 63 0 9 29 0 10 57 0 11 61 0
		 12 30 0 14 68 0 15 69 0 16 72 0 11 34 0 14 66 0 15 70 0 16 35 0 18 77 0 19 78 0 20 52 0
		 15 39 0 18 75 0 19 79 0 20 40 0 22 48 0 23 87 0 19 44 0 22 83 0 9 85 0 23 45 0 22 49 0
		 18 89 0 14 91 0 10 50 0 23 53 0 12 94 0 16 96 0 20 54 0 27 12 0 28 9 0 29 8 0 30 8 0
		 27 26 0 28 26 0 29 26 0 30 26 0 32 16 0 33 11 0 34 13 0 35 13 0 32 31 0 33 31 0 34 31 0
		 35 31 0 37 20 0 38 15 0 39 17 0 40 17 0 37 36 0 38 36 0 39 36 0 40 36 0 42 23 0 43 19 0
		 44 21 0 45 21 0 42 41 0 43 41 0 44 41 0 45 41 0 47 10 0 48 1 0 49 24 0 50 24 0 47 46 0
		 48 46 0 49 46 0 50 46 0 52 6 0 53 25 0 54 25 0 52 51 0 42 51 0 53 51 0 54 51 0 56 1 0
		 57 8 0 56 55 0 47 55 0 57 55 0 29 55 0 59 3 0 60 3 0 61 8 0 59 58 0 60 58 0 61 58 0
		 57 58 0 63 2 0 33 62 0 63 62 0 30 62 0 61 62 0 65 14 0 66 13 0 60 64 0 65 64 0 66 64 0
		 34 64 0 68 5 0 69 5 0 70 13 0 68 67 0 69 67 0 70 67 0 66 67 0 72 4 0 38 71 0 72 71 0
		 35 71 0 70 71 0 74 18 0 75 17 0 69 73 0 74 73 0 75 73 0 39 73 0 77 7 0 78 7 0 79 17 0
		 77 76 0 78 76 0 79 76 0 75 76 0 43 80 0 52 80 0 40 80 0 79 80 0 82 22 0 83 21 0 78 81 0
		 82 81 0 83 81 0 44 81 0 85 21 0 48 84 0 56 84 0 85 84 0 83 84 0 87 0 0 28 86 0 87 86 0
		 45 86 0 85 86 0 89 24 0 82 88 0;
	setAttr ".ed[166:191]" 77 88 0 89 88 0 49 88 0 91 24 0 74 90 0 68 90 0 91 90 0
		 89 90 0 65 92 0 59 92 0 50 92 0 91 92 0 94 25 0 87 93 0 27 93 0 94 93 0 53 93 0 96 25 0
		 63 95 0 32 95 0 96 95 0 94 95 0 72 97 0 37 97 0 54 97 0 96 97 0;
	setAttr -s 96 -ch 384 ".fc[0:95]" -type "polyFaces" 
		f 4 -20 -49 52 -56
		mu 0 4 51 19 47 46
		f 4 -27 -57 60 -64
		mu 0 4 57 25 54 52
		f 4 -34 -65 68 -72
		mu 0 4 63 31 60 58
		f 4 -40 -73 76 -80
		mu 0 4 69 37 66 64
		f 4 -44 -81 84 -88
		mu 0 4 75 17 71 70
		f 4 -48 29 91 -95
		mu 0 4 81 45 77 76
		f 4 -17 12 97 -101
		mu 0 4 50 15 83 82
		f 4 -18 13 104 -108
		mu 0 4 85 17 87 86
		f 4 -19 -58 109 -113
		mu 0 4 89 18 55 90
		f 4 -24 14 115 -119
		mu 0 4 56 18 88 92
		f 4 -25 20 122 -126
		mu 0 4 95 21 97 96
		f 4 -26 -66 127 -131
		mu 0 4 100 23 61 101
		f 4 -31 21 133 -137
		mu 0 4 62 23 99 104
		f 4 -32 27 140 -144
		mu 0 4 107 27 109 108
		f 4 -33 -74 144 -148
		mu 0 4 112 29 67 113
		f 4 -37 28 150 -154
		mu 0 4 68 29 111 115
		f 4 -38 34 155 -159
		mu 0 4 118 33 120 119
		f 4 -39 -50 160 -164
		mu 0 4 122 35 124 123
		f 4 -41 -149 165 -169
		mu 0 4 74 39 128 127
		f 4 -42 -132 170 -174
		mu 0 4 130 40 132 131
		f 4 -43 -114 174 -178
		mu 0 4 134 41 136 135
		f 4 -45 35 179 -183
		mu 0 4 80 43 138 137
		f 4 -46 15 184 -188
		mu 0 4 139 19 91 140
		f 4 -47 22 188 -192
		mu 0 4 142 44 144 143
		f 4 -5 0 53 -53
		mu 0 4 47 0 48 46
		f 4 49 16 54 -54
		mu 0 4 48 15 50 46
		f 4 50 -52 55 -55
		mu 0 4 50 14 51 46
		f 4 -7 1 61 -61
		mu 0 4 54 2 55 52
		f 4 57 23 62 -62
		mu 0 4 55 18 56 52
		f 4 58 -60 63 -63
		mu 0 4 56 20 57 52
		f 4 -9 2 69 -69
		mu 0 4 60 4 61 58
		f 4 65 30 70 -70
		mu 0 4 61 23 62 58
		f 4 66 -68 71 -71
		mu 0 4 62 26 63 58
		f 4 -11 3 77 -77
		mu 0 4 66 6 67 64
		f 4 73 36 78 -78
		mu 0 4 67 29 68 64
		f 4 74 -76 79 -79
		mu 0 4 68 32 69 64
		f 4 -6 -82 85 -85
		mu 0 4 71 1 73 70
		f 4 -35 40 86 -86
		mu 0 4 73 39 74 70
		f 4 82 -84 87 -87
		mu 0 4 74 38 75 70
		f 4 88 10 92 -92
		mu 0 4 77 12 79 76
		f 4 72 44 93 -93
		mu 0 4 79 43 80 76
		f 4 89 -91 94 -94
		mu 0 4 80 42 81 76
		f 4 95 5 98 -98
		mu 0 4 83 1 71 82
		f 4 80 17 99 -99
		mu 0 4 71 17 85 82
		f 4 96 -51 100 -100
		mu 0 4 85 14 50 82
		f 4 101 -103 105 -105
		mu 0 4 87 3 88 86
		f 4 -15 18 106 -106
		mu 0 4 88 18 89 86
		f 4 103 -97 107 -107
		mu 0 4 89 14 85 86
		f 4 -2 -109 110 -110
		mu 0 4 55 2 91 90
		f 4 -16 19 111 -111
		mu 0 4 91 19 51 90
		f 4 51 -104 112 -112
		mu 0 4 51 14 89 90
		f 4 102 7 116 -116
		mu 0 4 88 3 93 92
		f 4 113 24 117 -117
		mu 0 4 93 21 95 92
		f 4 114 -59 118 -118
		mu 0 4 95 20 56 92
		f 4 119 -121 123 -123
		mu 0 4 97 5 99 96
		f 4 -22 25 124 -124
		mu 0 4 99 23 100 96
		f 4 121 -115 125 -125
		mu 0 4 100 20 95 96
		f 4 -3 -127 128 -128
		mu 0 4 61 4 103 101
		f 4 -23 26 129 -129
		mu 0 4 103 25 57 101
		f 4 59 -122 130 -130
		mu 0 4 57 20 100 101
		f 4 120 9 134 -134
		mu 0 4 99 5 105 104
		f 4 131 31 135 -135
		mu 0 4 105 27 107 104
		f 4 132 -67 136 -136
		mu 0 4 107 26 62 104
		f 4 137 -139 141 -141
		mu 0 4 109 7 111 108
		f 4 -29 32 142 -142
		mu 0 4 111 29 112 108
		f 4 139 -133 143 -143
		mu 0 4 112 26 107 108
		f 4 -4 -89 145 -145
		mu 0 4 67 6 114 113
		f 4 -30 33 146 -146
		mu 0 4 114 31 63 113
		f 4 67 -140 147 -147
		mu 0 4 63 26 112 113
		f 4 138 11 151 -151
		mu 0 4 111 7 116 115
		f 4 148 37 152 -152
		mu 0 4 116 33 118 115
		f 4 149 -75 153 -153
		mu 0 4 118 32 68 115
		f 4 81 -96 156 -156
		mu 0 4 120 9 121 119
		f 4 -13 38 157 -157
		mu 0 4 121 35 122 119
		f 4 154 -150 158 -158
		mu 0 4 122 32 118 119
		f 4 -1 -160 161 -161
		mu 0 4 124 8 126 123
		f 4 -36 39 162 -162
		mu 0 4 126 37 69 123
		f 4 75 -155 163 -163
		mu 0 4 69 32 122 123
		f 4 -12 -138 166 -166
		mu 0 4 128 10 129 127
		f 4 -28 41 167 -167
		mu 0 4 129 40 130 127
		f 4 164 -83 168 -168
		mu 0 4 130 38 74 127
		f 4 -10 -120 171 -171
		mu 0 4 132 11 133 131
		f 4 -21 42 172 -172
		mu 0 4 133 41 134 131
		f 4 169 -165 173 -173
		mu 0 4 134 38 130 131
		f 4 -8 -102 175 -175
		mu 0 4 136 3 87 135
		f 4 -14 43 176 -176
		mu 0 4 87 17 75 135
		f 4 83 -170 177 -177
		mu 0 4 75 38 134 135
		f 4 159 4 180 -180
		mu 0 4 138 0 47 137
		f 4 48 45 181 -181
		mu 0 4 47 19 139 137
		f 4 178 -90 182 -182
		mu 0 4 139 42 80 137
		f 4 108 6 185 -185
		mu 0 4 91 2 141 140
		f 4 56 46 186 -186
		mu 0 4 141 44 142 140
		f 4 183 -179 187 -187
		mu 0 4 142 42 139 140
		f 4 126 8 189 -189
		mu 0 4 144 13 145 143
		f 4 64 47 190 -190
		mu 0 4 145 45 81 143
		f 4 90 -184 191 -191
		mu 0 4 81 42 142 143;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "59F36AC7-A44A-A2C7-C6E0-889A763EC811";
	setAttr -s 6 ".lnk";
	setAttr -s 6 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "8830F947-7F43-BCB8-EE7B-0EBE720787C2";
createNode displayLayer -n "defaultLayer";
	rename -uid "01B40DB9-EB49-2329-439E-449468597ED3";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "9643CEA4-774C-1B04-40CA-AC8747E87861";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "080A9AAC-7D44-FDF6-6EFD-B788A3E4F4E4";
	setAttr ".g" yes;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F7D1B70D-074D-1F67-A9D1-1A957A790D2D";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 1 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "29C0F924-6840-AB29-6C41-4AB20AAC4926";
createNode blinn -n "blinn1";
	rename -uid "1CB6FBA1-4244-6973-6A04-E3B144A22CBA";
	setAttr ".dc" 0.89932888746261597;
	setAttr ".c" -type "float3" 0.93379998 0.85339999 0 ;
	setAttr ".ambc" -type "float3" 1 1 1 ;
	setAttr ".ic" -type "float3" 0.023952097 0.023952097 0.023952097 ;
	setAttr ".sc" -type "float3" 0.12325155 0.62874252 0.12617339 ;
	setAttr ".rc" -type "float3" 0.3581 0.0561 0.0561 ;
createNode shadingEngine -n "blinn1SG";
	rename -uid "2B6A33F7-E643-B370-799C-00843702F9A9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "C73522E0-334D-F088-F269-44B5AF13E9F9";
createNode file -n "file1";
	rename -uid "1182C8DD-FF42-28EE-7D86-A0832957CA26";
	setAttr ".ftn" -type "string" "/Users/andrew/ncc_source/sceneKitTransformAnimTest/Maya-project/heartEyes.0083.png";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "791C2927-A144-BE30-4CB2-5DB502AC0317";
createNode blinn -n "blinn2";
	rename -uid "F62B00EC-0B43-BE31-160B-37A69B297782";
	setAttr ".c" -type "float3" 0.1329 0 0.7604 ;
createNode shadingEngine -n "blinn2SG";
	rename -uid "597D805A-5449-38F1-2192-B9B4C38DB3E8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "869FDD01-7748-8DB1-81D1-12B8DECA785C";
createNode blinn -n "blinn3";
	rename -uid "29ED45C9-A64F-3C7E-8632-E6A958E14441";
	setAttr ".c" -type "float3" 0.54000002 0 0.1962 ;
createNode shadingEngine -n "blinn3SG";
	rename -uid "7555DFEF-BE49-AA50-5028-9FB7302DF044";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "989FE5CF-FB49-D0A4-45EC-448DF602FE85";
createNode blinn -n "blinn4";
	rename -uid "39B1FDE8-8B4A-7043-D41E-E69212EDACDE";
	setAttr ".dc" 0.8657718300819397;
	setAttr ".c" -type "float3" 0.018999999 0 1 ;
createNode shadingEngine -n "blinn4SG";
	rename -uid "131B0D4E-DD4F-5811-4046-61B0857C323E";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
	rename -uid "3198DE13-4246-3BBD-393C-3ABC25C224F5";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "E0DD2125-A744-EAB6-94B7-54AC9D13CD71";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 421\n            -height 432\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 421\n            -height 432\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 421\n            -height 432\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 0\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 719\n            -height 779\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 1\n            -showAssignedMaterials 1\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n"
		+ "                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n"
		+ "                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n"
		+ "                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 0\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 719\\n    -height 779\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 0\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 719\\n    -height 779\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels yes -displayOrthographicLabels yes -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "1A416D8E-7648-8E9A-685E-F380D789FD4F";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode blendShape -n "blendShape1";
	rename -uid "EB85058A-0A46-741B-5C32-4E8C7054803D";
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr ".it[0].itg[0].iti[6000].ipt" -type "pointArray" 9 -1.633763313293457
		 0 0 1 1.633763313293457 0 0 1 0 2 0 1 -0.81688165664672852 0 0 1 0 2 0 1 0.81688165664672852
		 0 0 1 1 0 0 1 -1 0 0 1 0 2 0 1 ;
	setAttr ".it[0].itg[0].iti[6000].ict" -type "componentList" 8 "vtx[0:1]" "vtx[15]" "vtx[28]" "vtx[38]" "vtx[56]" "vtx[59]" "vtx[63]" "vtx[69]";
	setAttr ".it[0].siw" 1;
	setAttr ".mlid" 0;
	setAttr ".mlpr" 0;
	setAttr ".pndr[0]"  0;
	setAttr ".tgvs[0]" yes;
	setAttr ".tpvs[0]" yes;
	setAttr ".tgdt[0].cid" -type "Int32Array" 1 0 ;
	setAttr ".aal" -type "attributeAlias" {"weirdo","weight[0]"} ;
createNode groupId -n "groupId7";
	rename -uid "FEFAC992-1C4F-AE0D-309D-41A9EFE2A707";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "9F2BBFAD-1D46-53E5-582C-BABE75534FEF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[17]" "f[75]";
createNode groupId -n "groupId8";
	rename -uid "DAC52B6B-D84C-3E0E-58BF-899A2D3CBE62";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "9FDB13D8-9441-DBF2-3459-7FB02DC4D811";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[3]" "f[35]" "f[76:77]";
createNode groupId -n "groupId9";
	rename -uid "FCEF3A7E-1245-3D5B-9A0D-01866D430199";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "7284BCBA-3D4C-F7D6-48ED-4ABE470338CA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "f[0]" "f[8]" "f[24:26]" "f[48:50]";
createNode groupId -n "groupId10";
	rename -uid "CA88EE7D-514E-36BB-49B8-F3A476F117D5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "5752BC12-D64F-34DC-8F4E-7D99C062BE17";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 7 "f[1:2]" "f[9:12]" "f[14]" "f[27:34]" "f[51:59]" "f[62]" "f[64:68]";
createNode groupId -n "groupId11";
	rename -uid "BC07D767-8547-7AFE-4CD4-FDA32C59DD76";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "5051710E-AB43-13A5-3B22-B493290FCFBC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 9 "f[4:7]" "f[13]" "f[15:16]" "f[18:23]" "f[36:47]" "f[60:61]" "f[63]" "f[69:74]" "f[78:95]";
createNode tweak -n "tweak1";
	rename -uid "4FC440D6-EF40-A20F-86B9-AEABFE073DE3";
createNode objectSet -n "blendShape1Set";
	rename -uid "DADF19D2-7D4F-52E5-06A6-47816202FEE2";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "blendShape1GroupId";
	rename -uid "42004878-5A47-F661-57E5-74AF664CFA96";
	setAttr ".ihi" 0;
createNode groupParts -n "blendShape1GroupParts";
	rename -uid "0EA2CF79-814E-3FAF-E4A4-B288F5CE8894";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	rename -uid "FDA5C793-8F4B-936E-DFBE-9EBF960AAAF2";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId13";
	rename -uid "83FD6869-CA4F-A88C-2E58-89A1845CA48C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "4E46A53A-EE49-B563-F929-48940B75E839";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode animCurveTU -n "pCubeShape2Orig_aiOverrideLightLinking";
	rename -uid "39E9DBA3-294A-9DAF-EE71-06915671C1E2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiOverrideShaders";
	rename -uid "BA4315BF-1346-5A9A-0211-46BDA0BBFB9D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiUseFrameExtension";
	rename -uid "D3AEFC50-8641-D3F1-7CCC-E1B8E04A39A6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiFrameNumber";
	rename -uid "BBE36630-9041-1B05-B43E-2B9D2E9EA6B0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTU -n "pCubeShape2Orig_aiUseSubFrame";
	rename -uid "E42F6236-824B-B986-B2B5-BCBF261F091D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiFrameOffset";
	rename -uid "FE7B9BF5-4E41-5956-EF1D-55816B438C12";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTU -n "pCubeShape2Orig_aiOverrideNodes";
	rename -uid "574B4D0E-DA4A-4BD8-7852-448AABE212BB";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiOverrideReceiveShadows";
	rename -uid "B2F7807F-6D48-B7BF-4F7B-86B9A0CDCD02";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiOverrideDoubleSided";
	rename -uid "FFF885A1-A140-6F1A-1F76-659E40CAD545";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiOverrideSelfShadows";
	rename -uid "11B5FEA0-014C-C8D4-8F57-4B9E1991F0F1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiOverrideOpaque";
	rename -uid "0039F75C-5D4A-42AC-D86B-DA8DC0082E7B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "pCubeShape2Orig_aiOverrideMatte";
	rename -uid "C0368A77-DE4F-6487-3378-C88FB45E03E5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "blendShape1_weirdo";
	rename -uid "F2D0C688-024B-834B-5147-FF9E9735C292";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  1 0 25 0.78873240947723389 37 1 38 1 93 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_0__xValue";
	rename -uid "E8EEBB8C-214C-CC4B-D0BB-5ABCAE265DF9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_0__yValue";
	rename -uid "810906AB-C242-6190-7297-20BA6AB51B4B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_0__zValue";
	rename -uid "CCBBBF22-B941-8BAC-4E83-EA86E1DE228C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_1__xValue";
	rename -uid "DC277E8A-A247-6275-AB52-DA8ACF5E7C7E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_1__yValue";
	rename -uid "5230B6B9-BE43-D080-859E-DDAE52C5EF16";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_1__zValue";
	rename -uid "11D413D1-6546-273B-FB8B-D1B74A6F9A27";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_2__xValue";
	rename -uid "4891239B-254B-8521-83B5-349BBA199F99";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_2__yValue";
	rename -uid "3213EAF0-8249-A49C-B691-F6A1450A50DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_2__zValue";
	rename -uid "5EA75A21-614F-8583-9CFA-41A880081E6E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_3__xValue";
	rename -uid "E1C9935E-5848-B710-A19B-4081F6A2DA4B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_3__yValue";
	rename -uid "2616D1DF-3A43-96BF-EA76-55BB9D6DE19F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_3__zValue";
	rename -uid "1710294F-5241-2EFC-42C3-71A6C6903815";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_4__xValue";
	rename -uid "2F1A0207-CE4E-66F0-34BC-94810090A312";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_4__yValue";
	rename -uid "2175797C-CE4B-0EE2-D721-659B1B85084C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_4__zValue";
	rename -uid "ABD7ED5E-5D43-F4A5-8D70-BA9C19DCDA83";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_5__xValue";
	rename -uid "D06713A3-084A-5994-75EC-8AB92BC1F487";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_5__yValue";
	rename -uid "749B762E-D340-214A-5833-BDB042980BD3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_5__zValue";
	rename -uid "87E4202A-9F46-E378-3F54-22A8DA94BF32";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_6__xValue";
	rename -uid "868FC686-5442-0585-AE44-B9A0BBAE2AE3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_6__yValue";
	rename -uid "4BC37D1B-E74D-325C-7549-3DBBB9F989D8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_6__zValue";
	rename -uid "F5708358-2244-B931-3E63-22A8E50E9CC6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_7__xValue";
	rename -uid "65FF90E7-4244-2209-DD37-6BA8A18ADF5E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_7__yValue";
	rename -uid "3AB80895-D344-9FC3-1C19-9BA6DA8B7890";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_7__zValue";
	rename -uid "A41BD60A-3044-8107-03D4-608743CC377C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_8__xValue";
	rename -uid "BF5FC6EA-0742-EC20-0EA7-D18978361DE2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_8__yValue";
	rename -uid "6860FE6B-AB48-6F10-8860-9F835705A5AA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_8__zValue";
	rename -uid "3653AD63-954B-3B26-DEE9-00BF57AA6AEB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_9__xValue";
	rename -uid "D533EE76-BA46-38F4-F4FC-928C0033E6E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_9__yValue";
	rename -uid "E6459386-CA4B-3F85-B9EE-0BBF4F7E3E68";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_9__zValue";
	rename -uid "19D9EA31-E844-2CBA-7F0D-B49CDB02D2FE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_10__xValue";
	rename -uid "7D5BCA74-9844-EE50-1001-458AE2D3D758";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_10__yValue";
	rename -uid "4880AA99-0D4D-66BB-46B4-57A4E74350FF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_10__zValue";
	rename -uid "925E2507-7B49-28D0-C035-E88BDBE79B7F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_11__xValue";
	rename -uid "488E18FE-0642-5458-8C47-90A7506CB92B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_11__yValue";
	rename -uid "D5298AFE-674E-2FF9-61EA-AF9183BE3A60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_11__zValue";
	rename -uid "3C338154-434E-EDD1-D670-E1B80EADBF00";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_12__xValue";
	rename -uid "9507219F-3844-84F1-42CF-C390359C6048";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_12__yValue";
	rename -uid "9FF3329B-A14D-6251-CE49-6387790C8585";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_12__zValue";
	rename -uid "836A9B10-374A-CE5C-F567-82AD51EA5140";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_13__xValue";
	rename -uid "828D9DD1-3547-EA8A-0B3E-DFB856CDEED1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_13__yValue";
	rename -uid "F8B2085C-DF41-F47C-87E0-1CBED7FB59BD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_13__zValue";
	rename -uid "D6783961-AF4B-41E7-7B7B-88A159688822";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_14__xValue";
	rename -uid "692C2F8D-B145-8741-4083-ACAF30972933";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_14__yValue";
	rename -uid "84D1213D-A342-8F04-5CC7-0CB62376678C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_14__zValue";
	rename -uid "61E561E9-9B46-8E5E-3000-079FCE719660";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_15__xValue";
	rename -uid "470738F3-3A41-A69B-A2C0-53B539189C5E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_15__yValue";
	rename -uid "49C8332F-584F-948A-693C-22B0CFE2E34D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_15__zValue";
	rename -uid "8618E570-AD40-FEE3-7446-A6A778CB1C95";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_16__xValue";
	rename -uid "8D701C46-5A4A-D428-9FD1-D2A736307758";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_16__yValue";
	rename -uid "922A32D9-F140-06E6-E3F6-6E9279030A66";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_16__zValue";
	rename -uid "BF486CD7-7D4B-5AD4-F991-34A5BF437926";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_17__xValue";
	rename -uid "318B6B0A-8B4D-07F5-F26D-DBBEAA8D3C19";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_17__yValue";
	rename -uid "E3C4DF50-D54D-0AA7-04A4-699ACC1107F4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_17__zValue";
	rename -uid "D0F40FD2-EA46-13C5-504B-D6BDB9ABBEB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_18__xValue";
	rename -uid "78C3002F-8F4F-7DDB-4A17-1C82EAF15A4D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_18__yValue";
	rename -uid "EB9F560F-EF4F-F9E0-74FD-F4996E5CBED1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_18__zValue";
	rename -uid "C3B9397B-3644-5BD7-C553-8F81A3BBF71A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_19__xValue";
	rename -uid "F26ABBB5-7440-5997-F8AE-B4AD352A722D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_19__yValue";
	rename -uid "F896A869-5B48-80A4-EA5F-D7B5774B4489";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_19__zValue";
	rename -uid "FE2DC64F-6343-C043-4C84-308083EEDF80";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_20__xValue";
	rename -uid "3ADD7C1F-6E4F-2194-AEBC-B4819C0BFFEC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_20__yValue";
	rename -uid "ACBE3ECF-7A4F-BA65-D056-26BA117CFA1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_20__zValue";
	rename -uid "F0D2B036-DD4C-6184-6641-B9A98BC1CF3C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_21__xValue";
	rename -uid "F6A3C709-AF4C-2155-3B51-BD8D471C58A9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_21__yValue";
	rename -uid "E78F6F19-224F-F0CE-D0EF-018376C144AD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_21__zValue";
	rename -uid "48B7F396-1D44-E523-22D3-0D814394D75B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_22__xValue";
	rename -uid "FEC96DC0-3C4D-898B-5A6C-50A8220D4295";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_22__yValue";
	rename -uid "2ADC5867-4147-CCB4-30F2-0197EC1CB3C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_22__zValue";
	rename -uid "2D69C4BB-5549-87CF-1317-609490419227";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_23__xValue";
	rename -uid "66831087-D84A-222A-9355-689C6DE08D59";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_23__yValue";
	rename -uid "2D731260-4048-E258-32C4-2BBD483B9930";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_23__zValue";
	rename -uid "E7C004C5-6740-6983-4DAB-0DA82E0CF3ED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_24__xValue";
	rename -uid "FF3E1C9B-AA42-0694-1580-FDAB62093BDA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_24__yValue";
	rename -uid "9491C640-774A-5B82-157F-35B1ED6835AC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_24__zValue";
	rename -uid "5F0C3183-364D-F90A-F7DC-9EB9DCD8E777";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_25__xValue";
	rename -uid "2FE9AF6E-8B4B-D7D1-6995-78AF1F911A70";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_25__yValue";
	rename -uid "60D8EF02-8D43-37CB-D1C8-D6AEF0518B83";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_25__zValue";
	rename -uid "3D5800A2-B148-3FAA-3A21-8F859CFF42D1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_26__xValue";
	rename -uid "1E427871-6C41-2689-FBC5-C3B1C2D01FCA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_26__yValue";
	rename -uid "DC5D2FF5-3548-8C18-D400-86AA5B767237";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_26__zValue";
	rename -uid "50728255-E342-F481-EAF5-CE883535A6DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_27__xValue";
	rename -uid "67ADC2F5-3E40-27ED-F5F9-D3B64DDA3FBD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_27__yValue";
	rename -uid "9E589428-7244-AF8A-FAEA-07859974D430";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_27__zValue";
	rename -uid "11E57E38-A542-F539-C65A-1F9C9BE36EED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_28__xValue";
	rename -uid "F292CAD4-D24D-C97B-6D4B-70868871E620";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_28__yValue";
	rename -uid "A30C6B49-344F-0AEC-0108-F4AA95F089BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_28__zValue";
	rename -uid "06FD7EA2-F248-1321-B8DF-85A3B315239C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_29__xValue";
	rename -uid "01A0236D-5F4C-8356-4472-8093DBBD46F2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_29__yValue";
	rename -uid "34F7AFAF-184C-89C9-C0D5-4EA1F778CF2A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_29__zValue";
	rename -uid "B1DD817F-F642-097A-34B4-74B8377F4CC3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_30__xValue";
	rename -uid "6D80D347-BA4C-D6A0-F238-98A78C678D73";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_30__yValue";
	rename -uid "8E40090B-E542-DED7-5090-108791F83F6B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_30__zValue";
	rename -uid "DC661A50-6A4D-CF83-ECB5-E3A646EA0D31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_31__xValue";
	rename -uid "0D484DC2-094B-EBED-82CB-DCABAB9D14FE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_31__yValue";
	rename -uid "9AFEEB5C-0A40-2431-BDE1-758D1CB16099";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_31__zValue";
	rename -uid "6E87F957-7443-0ACD-F441-B29648F1F07D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_32__xValue";
	rename -uid "C447C569-EC45-70A7-7B89-1FB375D5D0E2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_32__yValue";
	rename -uid "9D62BAD6-4A45-6448-D9EC-D7B3D8D41E8D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_32__zValue";
	rename -uid "F85ACD93-B644-07DA-5F45-3FB900B8FED1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_33__xValue";
	rename -uid "359FF174-7F49-C4E3-004C-F3B540698A03";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_33__yValue";
	rename -uid "BC4DEB4A-1E43-0065-D24F-71BF72C3F819";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_33__zValue";
	rename -uid "E86C1A1A-6D42-07FE-9C39-BCB48CA7EF81";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_34__xValue";
	rename -uid "EE94947C-494E-0DA4-2346-8C9F5E947C8C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_34__yValue";
	rename -uid "BAFD0C8A-2643-9E86-90D7-788883F6F5D9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_34__zValue";
	rename -uid "78A92EBC-464A-754C-E33F-BE852C5B06EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_35__xValue";
	rename -uid "567C9D8C-174F-243F-1D1C-47B6B5074A61";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_35__yValue";
	rename -uid "5F64CEF9-0F41-DF46-9A01-F5B751C623F7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_35__zValue";
	rename -uid "5CAFB845-5D48-34CC-809D-FDA0B65BB004";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_36__xValue";
	rename -uid "BD250E6A-4A4B-3010-AF75-379D4B0D3398";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_36__yValue";
	rename -uid "BE8FE294-DD44-28A4-AA86-448A6188514E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_36__zValue";
	rename -uid "11A8BA32-DC4A-3093-4ED0-F290AF66EAF0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_37__xValue";
	rename -uid "D2B5B31B-AD47-08D5-561A-9DB3C2C2358E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_37__yValue";
	rename -uid "269B2F8B-EA4B-277E-F8B2-5E854C3722F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_37__zValue";
	rename -uid "4416D4CC-944A-DBFD-32F5-E5B2468FC323";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_38__xValue";
	rename -uid "0806BFEF-0546-F767-3DCD-DA989D50EFAF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_38__yValue";
	rename -uid "08C16FF9-024F-A16D-3512-71AD9D4ADFDA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_38__zValue";
	rename -uid "F6977331-3948-24B4-8801-FD9C786DD8BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_39__xValue";
	rename -uid "3709EBD9-DD41-454F-CA75-E7806A4E9774";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_39__yValue";
	rename -uid "0748D408-5E41-DCA7-278D-E0B42291AAEA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_39__zValue";
	rename -uid "2209F33C-A740-6715-2B22-BC8400DD96A5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_40__xValue";
	rename -uid "9C3F9B35-4A44-789A-165E-57AA7FFF5DA6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_40__yValue";
	rename -uid "794F9A82-624C-B28B-857A-08A3302F1596";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_40__zValue";
	rename -uid "A1760223-E247-4D89-F6D7-AD829F5C07B3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_41__xValue";
	rename -uid "C3DF158E-A146-77FC-ACB5-318FE57CF4EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_41__yValue";
	rename -uid "13CF02E1-A249-CC0E-452A-7B88C375DCFA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_41__zValue";
	rename -uid "AC86F647-2F4B-0579-B3CA-48908613F9B4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_42__xValue";
	rename -uid "0991FD98-1D48-F9FC-C5AA-EC9366A8BA17";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_42__yValue";
	rename -uid "20F4F43C-804E-E65A-9BA4-E1AF4CEB6ADB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_42__zValue";
	rename -uid "4C69444C-B647-9C83-21DB-0A9413A8FD6F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_43__xValue";
	rename -uid "0B3AF867-994B-ED50-2006-13A8AAD784E7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_43__yValue";
	rename -uid "F2F15F19-1B44-C150-39DF-EDAE7EF83E94";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_43__zValue";
	rename -uid "36365B1F-A14D-2977-1407-E09555A55F55";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_44__xValue";
	rename -uid "0E703FE1-7C4F-D641-80AF-25A389ECDE89";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_44__yValue";
	rename -uid "642B5D19-F441-203E-3FE7-D4B32EF5940E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_44__zValue";
	rename -uid "16FF6738-8D47-A434-C990-88969210D3E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_45__xValue";
	rename -uid "8B18ACB7-4343-6F28-4368-04939E483D6F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_45__yValue";
	rename -uid "9C14001E-3146-862D-B284-C18A070922ED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_45__zValue";
	rename -uid "2DA775BD-4640-312E-4259-99B0462972E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_46__xValue";
	rename -uid "3B214A46-7A4E-81CF-2423-F5896C04D64A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_46__yValue";
	rename -uid "88E479DD-124A-EB0A-6EDD-3DA17B723861";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_46__zValue";
	rename -uid "928CA06B-7E42-88FE-5106-EEAD05712E13";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_47__xValue";
	rename -uid "766D688A-1049-B882-9E66-5FA60B9D5635";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_47__yValue";
	rename -uid "F0DE3DC2-894C-35E5-A126-9D9DD5804B8A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_47__zValue";
	rename -uid "E3739F84-3B42-AA00-788B-5A9563A976EF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_48__xValue";
	rename -uid "B5A7C383-5742-60C0-1A46-80B280D6AE79";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_48__yValue";
	rename -uid "BF1620FB-484F-6F9C-3494-208096CFBE48";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_48__zValue";
	rename -uid "6F9F534D-9143-7F5B-DF20-7F9C14D3CE4A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_49__xValue";
	rename -uid "C24B77D7-8E4C-56F3-6B6A-E28AF8ECDC72";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_49__yValue";
	rename -uid "0BE98328-4946-68E4-FEFD-69A33758C486";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_49__zValue";
	rename -uid "1240C689-554C-7A25-FDC8-D9A24534AE13";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_50__xValue";
	rename -uid "B052D3DD-C54D-E15F-3325-B3994E742C42";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_50__yValue";
	rename -uid "6198FFB9-0541-5066-7829-9584ADDCB5A1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_50__zValue";
	rename -uid "CC439080-DC47-15DC-F6F7-3DB99D1837C7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_51__xValue";
	rename -uid "7C512909-9C4B-AEEE-412C-908C68FF072A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_51__yValue";
	rename -uid "114056C2-D54D-F7AD-AB4F-94B8561A9D22";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_51__zValue";
	rename -uid "31B70B48-594A-5326-1772-459CFAA83EB1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_52__xValue";
	rename -uid "CA2F9966-A346-EF04-477B-AA9DC5409F83";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_52__yValue";
	rename -uid "55AB68FE-C142-F4E3-ADC1-8E84EBCDACF8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_52__zValue";
	rename -uid "CDDA03FC-5D4C-8A89-448C-DA8BC73D876E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_53__xValue";
	rename -uid "0E604FAA-E643-0577-E88C-2A8ECCC79AE6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_53__yValue";
	rename -uid "989B4F80-DF4E-4558-9A6F-3ABB49528778";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_53__zValue";
	rename -uid "B88A6E20-7F47-FD48-7E2A-B8A33E278A67";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_54__xValue";
	rename -uid "723793E7-6346-E47B-8DFF-76ACD8FDDCDD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_54__yValue";
	rename -uid "237817D7-E146-3F7E-B69D-2DA19E79D4A9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_54__zValue";
	rename -uid "D0F44365-E14C-E795-66D1-6885A29BD536";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_55__xValue";
	rename -uid "3640B8D1-7C41-836B-84BF-A7A5E39B44C8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_55__yValue";
	rename -uid "C9CDB8D7-6D47-4135-541B-418032D319AD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_55__zValue";
	rename -uid "32D42A3A-0F48-99DE-FB91-F485B0CBCF2F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_56__xValue";
	rename -uid "95437F48-D34C-3686-F088-ECB0909867E2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_56__yValue";
	rename -uid "B8AA0AA0-AE46-6C82-E41F-18934478D9E8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_56__zValue";
	rename -uid "CB432B8B-854B-98CF-0F8A-43B434DF08B4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_57__xValue";
	rename -uid "21125890-6743-6E56-389D-318BAE2AE25B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_57__yValue";
	rename -uid "58A8FFBF-FF47-DCC3-A443-43B464614050";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_57__zValue";
	rename -uid "AF9BA243-7649-8311-88F8-0CA5E7936882";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_58__xValue";
	rename -uid "9E49BA8B-0A4E-4586-2FD5-ADA39AE31147";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_58__yValue";
	rename -uid "93F93A50-CA42-E797-BC35-5E98BFF29B8C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_58__zValue";
	rename -uid "3B0860B6-9946-C2BB-3ADC-AB92E969F6B0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_59__xValue";
	rename -uid "1BDBA436-584D-5DD3-9A42-178B57282A33";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_59__yValue";
	rename -uid "E0BFCD26-2645-C77E-BF87-CE91A03CC5AB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_59__zValue";
	rename -uid "6407CBF2-8749-6BCB-FC8C-6993B8A83E90";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_60__xValue";
	rename -uid "4E647B0E-6747-3929-680F-778BAB5B622B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_60__yValue";
	rename -uid "45BF73F5-A64C-3E67-41A5-F68838026309";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_60__zValue";
	rename -uid "B1E03818-A149-2AD3-79B4-54B536871C29";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_61__xValue";
	rename -uid "A65E4863-2D43-D588-429E-7888EA015C63";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_61__yValue";
	rename -uid "4782B143-F649-AD01-F669-DAA75D2D228B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_61__zValue";
	rename -uid "187AB04D-B54B-02BB-A8AE-979FFF6C6453";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_62__xValue";
	rename -uid "10972E08-A241-EEEA-8C0F-E2A9680C27B5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_62__yValue";
	rename -uid "E4F8DDDF-694D-B898-54AC-6C82A4C8737D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_62__zValue";
	rename -uid "E11D874C-6540-20BE-B9B2-DBA3262473FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_63__xValue";
	rename -uid "18403B91-AC48-A032-6228-E98CB54444A3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_63__yValue";
	rename -uid "AF77E973-C741-B5F1-D677-1AB831F6D36A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_63__zValue";
	rename -uid "46E63D2F-7844-B2A4-12FB-29A6E7231350";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_64__xValue";
	rename -uid "4AE15BCF-084E-1CB2-5380-5FAC11135AA8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_64__yValue";
	rename -uid "F7D1038D-6544-0E62-FEE2-F38FF2B62900";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_64__zValue";
	rename -uid "FEB01774-984B-2F37-0022-5794B29E6779";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_65__xValue";
	rename -uid "D8E18CA2-A144-0C1B-A33A-999225478945";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_65__yValue";
	rename -uid "BFE4185B-A843-4D9D-A71F-18BDAC346207";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_65__zValue";
	rename -uid "3763631B-3A4B-31B4-E918-D7AFD5218B3F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_66__xValue";
	rename -uid "25C99B67-8A46-3DB7-A9FF-B2A0785055D6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_66__yValue";
	rename -uid "4CFF0ABE-E946-87C9-EC8F-C8A62F7116BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_66__zValue";
	rename -uid "B493E4A9-8041-71CB-7F65-6AA949359407";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_67__xValue";
	rename -uid "31FAA2C7-CB4E-BD7A-0982-1BB9BEAFA2E2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_67__yValue";
	rename -uid "B195AAD7-C54E-D917-46CF-47B8B0823058";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_67__zValue";
	rename -uid "27787262-CF4A-C9C2-5CFD-789E0ACBF250";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_68__xValue";
	rename -uid "CD83D72B-A94E-AEA4-7721-EB83A3F65C21";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_68__yValue";
	rename -uid "9BD07FFD-7744-6A8E-08BF-AF96DC05D533";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_68__zValue";
	rename -uid "E1B99443-1D4B-4D20-9EEE-6790E4FA2B74";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_69__xValue";
	rename -uid "E22DE927-9C40-81F3-23D2-E8817B82033F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_69__yValue";
	rename -uid "7872A277-8D48-8E97-DC11-CD9B163FE022";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_69__zValue";
	rename -uid "7903AD69-6241-0FF3-FB13-A88ACA1C367E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_70__xValue";
	rename -uid "8BE07B30-BE47-D98D-108F-4D8DE8638280";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_70__yValue";
	rename -uid "9A793877-FB40-E95A-1ADE-E4842BF1B8C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_70__zValue";
	rename -uid "CB6873DE-3E4B-DFA4-0319-1F9240D33139";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_71__xValue";
	rename -uid "6FF64D8F-A14F-B3E0-79B1-3E89C04B9E2B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_71__yValue";
	rename -uid "8B722600-2942-7876-A634-94AF13156E6D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_71__zValue";
	rename -uid "8BA54E74-0746-0780-77E7-09B7D2FE8962";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_72__xValue";
	rename -uid "F6977B59-8149-4B49-3000-A7888CCA4F67";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_72__yValue";
	rename -uid "6707A55F-F844-C005-4B09-809A6B35F05A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_72__zValue";
	rename -uid "26D88FAD-D94A-8DAD-71F9-30BFAB58EC6C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_73__xValue";
	rename -uid "B3012860-A645-45B6-0492-A1A5D704D1F8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_73__yValue";
	rename -uid "D1A3208A-484F-8CCD-3EEF-369B49684E59";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_73__zValue";
	rename -uid "EF4D5BAE-D44B-CFB9-72A2-ECAA192AD9E1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_74__xValue";
	rename -uid "D5C5248A-3043-552C-4166-1AB7FACE4B4F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_74__yValue";
	rename -uid "799AEE6B-084A-8982-33CB-6B80446C1663";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_74__zValue";
	rename -uid "DF965CA2-A947-4EA8-EC85-5AB06CB1DE6F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_75__xValue";
	rename -uid "E83CF34A-E84F-D43E-1760-22AD89A2736B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_75__yValue";
	rename -uid "5F338557-8A40-9BC7-8B59-4EBC6D16650D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_75__zValue";
	rename -uid "777AAADD-144A-4188-D407-7FB8CF08E014";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_76__xValue";
	rename -uid "EAD2CB91-F14E-4B85-BAD9-D691454A3E51";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_76__yValue";
	rename -uid "17BC4B77-D546-5D2B-465E-E181838E6735";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_76__zValue";
	rename -uid "F260AC09-F64E-4A3E-B699-DCB4AEB28D46";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_77__xValue";
	rename -uid "85845BAD-1A4C-E318-A2A0-8EB7F16C7D21";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_77__yValue";
	rename -uid "4A27823D-E143-69F5-B2BF-79ABCD2E84D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_77__zValue";
	rename -uid "D0FCB04E-3647-0122-F38F-A193EA68C2A3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_78__xValue";
	rename -uid "E53E5B28-4C47-D661-E906-77A892AD2711";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_78__yValue";
	rename -uid "17788138-504C-BEA4-F7DB-03A4707A18F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_78__zValue";
	rename -uid "E7A50001-D643-5C24-95EB-47BFCB643314";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_79__xValue";
	rename -uid "377A7C55-AB46-44C8-CCFF-6A9F1635215D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_79__yValue";
	rename -uid "359F68A3-784A-D523-6AE6-E7BF189BD086";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_79__zValue";
	rename -uid "B2909CF6-4348-C61F-C08B-789FC9ECCCCF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_80__xValue";
	rename -uid "EEB33174-2140-C631-A5D4-ECB45A97D583";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_80__yValue";
	rename -uid "ABF63034-AD42-4CAD-7035-B88DD9CA11CC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_80__zValue";
	rename -uid "FBC261AE-2F47-BBC9-D4C5-1E8CDFEC2C4E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_81__xValue";
	rename -uid "7E9CEAA2-CA42-15FB-A6EB-4F8202F146D6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_81__yValue";
	rename -uid "51D63FC4-A341-4AFC-84F2-9AB378DB351B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_81__zValue";
	rename -uid "66AF33C1-C442-7E0D-2B0B-348EC4CD5882";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_82__xValue";
	rename -uid "90EF6D8C-9645-DC79-CC1E-F99822C62D36";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_82__yValue";
	rename -uid "560160C3-AB4C-F7FC-515F-FD8256A7D89F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_82__zValue";
	rename -uid "6DEA8984-C542-9D55-4EB1-D883F56CEC4E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_83__xValue";
	rename -uid "29EC52D5-8549-60F4-9E39-438E83F2EF30";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_83__yValue";
	rename -uid "C4FAA877-AE4E-7BDB-242D-81B6E3685B45";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_83__zValue";
	rename -uid "75EC8E78-3F4A-534F-8223-A980ECB578BD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_84__xValue";
	rename -uid "11E4CBE1-B24F-F906-9194-7CA0FB7B81FE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_84__yValue";
	rename -uid "B9A45D30-974F-0416-F368-30AFE24B59EE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_84__zValue";
	rename -uid "6D7C533B-5A46-39F4-EA91-67AC4B007F91";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_85__xValue";
	rename -uid "EAA63E58-1A42-B595-5E52-F0BE10BF26D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_85__yValue";
	rename -uid "5007D4FF-614F-6946-B35E-D9A416D087F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_85__zValue";
	rename -uid "E41A7F0E-904F-9AA7-8E87-06A319149D4F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_86__xValue";
	rename -uid "13B593BA-8D44-212E-71A6-65829EC9993A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_86__yValue";
	rename -uid "32AC5B01-FD47-7537-7F37-74B04698131B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_86__zValue";
	rename -uid "6752EC27-F749-59C2-30BF-CF9ADA8A1B07";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_87__xValue";
	rename -uid "58108700-0B40-5B7F-25FA-DFBC52DBDA31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_87__yValue";
	rename -uid "6C5ECE11-CB41-C5E7-7193-85A741144EED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_87__zValue";
	rename -uid "34576EDA-5345-AE34-A28B-558DF4A7A165";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_88__xValue";
	rename -uid "779350D0-5345-4850-CA40-87A0CC1748C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_88__yValue";
	rename -uid "6D57E7C4-774D-AC1E-28A9-108A68CF888A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_88__zValue";
	rename -uid "CF22150F-604D-117D-7539-ACBCFB923F79";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_89__xValue";
	rename -uid "7FF92A59-2E45-122D-1DE6-29885CE6B1B0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_89__yValue";
	rename -uid "060D056A-9443-DE08-0A1B-668E3B98C223";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_89__zValue";
	rename -uid "AD9E7094-AC44-E88D-752F-47B19BFF9BE2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_90__xValue";
	rename -uid "5B17BC62-BB45-7C1C-4AAB-8FB3E8C4192F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_90__yValue";
	rename -uid "CD4F1B46-9542-D515-CCA0-5190AE74C6B9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_90__zValue";
	rename -uid "0238D6F9-0747-3378-662F-E9889E871F06";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_91__xValue";
	rename -uid "E7C164D8-9747-416A-27D1-408641D6F0FA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_91__yValue";
	rename -uid "203AA742-7D49-9B94-8AEF-6A9A687F2D26";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_91__zValue";
	rename -uid "92B3B8B6-914D-BC19-3773-F493BA61BDB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_92__xValue";
	rename -uid "EC82985A-8C49-D085-710B-45A3192B0375";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 25 1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_92__yValue";
	rename -uid "0C17D4D1-FE4D-ACDE-A971-00BD7EE01839";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_92__zValue";
	rename -uid "7D65B2C5-FC45-6FC6-4780-3DAD9778072D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_93__xValue";
	rename -uid "D5AC5634-474A-E755-4F00-11A9892F7185";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_93__yValue";
	rename -uid "C9D69C13-BA45-DBD2-022D-6382DAEC7A38";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_93__zValue";
	rename -uid "DBF15D33-D847-00FC-7172-09A451577161";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_94__xValue";
	rename -uid "28FECDE7-254D-D0A1-11A5-ABAD85C3A3F6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_94__yValue";
	rename -uid "5C20CC8F-5849-920F-CD1A-CEAE61D4CDA0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_94__zValue";
	rename -uid "5202CACF-6944-C25B-9530-A4A7988BAAD4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_95__xValue";
	rename -uid "F2B01BB4-6349-81E1-0C1D-AD802CC22B2B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_95__yValue";
	rename -uid "283D6CA3-E44A-442F-1747-AA8F2FCB1650";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_95__zValue";
	rename -uid "58C5CF2B-5F47-52DA-49F6-F7964417DD89";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_96__xValue";
	rename -uid "1E6D636D-BE4F-D5AC-786A-FCBBAD635517";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_96__yValue";
	rename -uid "4F013DB7-334A-DD8D-143B-3FB4C96FE1B1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_96__zValue";
	rename -uid "A05C1CF4-D14F-D0FC-CD30-25AF99AB7A22";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_97__xValue";
	rename -uid "153E6626-3E4E-AAD4-2AD0-099E16B1830C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -1 25 -1;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_97__yValue";
	rename -uid "382082E3-FC40-60F3-93BA-148B66951CDF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0.5 25 0.5;
createNode animCurveTL -n "pCubeShape2Orig_controlPoints_97__zValue";
	rename -uid "F49BC83C-914F-23E7-CD7F-4394ED7C53A0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -0.5 25 -0.5;
createNode animCurveTL -n "pCube2_translateX";
	rename -uid "60303722-F943-C70E-8083-6F81B1CE611A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCube2_translateY";
	rename -uid "0E7E1A7B-1741-8A3D-C358-9283C980DF2E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
createNode animCurveTL -n "pCube2_translateZ";
	rename -uid "D4F8FF7E-B043-BE87-A073-4DBF7FF206A0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 25 0;
select -ne :time1;
	setAttr ".o" 106;
	setAttr ".unw" 106;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 6 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 8 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "pCube2_translateX.o" "pCube2.tx";
connectAttr "pCube2_translateY.o" "pCube2.ty";
connectAttr "pCube2_translateZ.o" "pCube2.tz";
connectAttr "groupId7.id" "pCubeShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupId8.id" "pCubeShape2.iog.og[1].gid";
connectAttr "blinn1SG.mwc" "pCubeShape2.iog.og[1].gco";
connectAttr "groupId9.id" "pCubeShape2.iog.og[2].gid";
connectAttr "blinn2SG.mwc" "pCubeShape2.iog.og[2].gco";
connectAttr "groupId10.id" "pCubeShape2.iog.og[3].gid";
connectAttr "blinn3SG.mwc" "pCubeShape2.iog.og[3].gco";
connectAttr "groupId11.id" "pCubeShape2.iog.og[4].gid";
connectAttr "blinn4SG.mwc" "pCubeShape2.iog.og[4].gco";
connectAttr "blendShape1GroupId.id" "pCubeShape2.iog.og[5].gid";
connectAttr "blendShape1Set.mwc" "pCubeShape2.iog.og[5].gco";
connectAttr "groupId13.id" "pCubeShape2.iog.og[6].gid";
connectAttr "tweakSet1.mwc" "pCubeShape2.iog.og[6].gco";
connectAttr "blendShape1.og[0]" "pCubeShape2.i";
connectAttr "tweak1.vl[0].vt[0]" "pCubeShape2.twl";
connectAttr "pCubeShape2Orig_aiOverrideLightLinking.o" "pCubeShape2Orig.ai_override_light_linking"
		;
connectAttr "pCubeShape2Orig_aiOverrideShaders.o" "pCubeShape2Orig.ai_override_shaders"
		;
connectAttr "pCubeShape2Orig_aiUseFrameExtension.o" "pCubeShape2Orig.ai_use_frame_extension"
		;
connectAttr "pCubeShape2Orig_aiFrameNumber.o" "pCubeShape2Orig.ai_frame_number";
connectAttr "pCubeShape2Orig_aiUseSubFrame.o" "pCubeShape2Orig.ai_use_sub_frame"
		;
connectAttr "pCubeShape2Orig_aiFrameOffset.o" "pCubeShape2Orig.ai_frame_offset";
connectAttr "pCubeShape2Orig_aiOverrideNodes.o" "pCubeShape2Orig.ai_override_nodes"
		;
connectAttr "pCubeShape2Orig_aiOverrideReceiveShadows.o" "pCubeShape2Orig.ai_override_receive_shadows"
		;
connectAttr "pCubeShape2Orig_aiOverrideDoubleSided.o" "pCubeShape2Orig.ai_override_double_sided"
		;
connectAttr "pCubeShape2Orig_aiOverrideSelfShadows.o" "pCubeShape2Orig.ai_override_self_shadows"
		;
connectAttr "pCubeShape2Orig_aiOverrideOpaque.o" "pCubeShape2Orig.ai_override_opaque"
		;
connectAttr "pCubeShape2Orig_aiOverrideMatte.o" "pCubeShape2Orig.ai_override_matte"
		;
connectAttr "pCubeShape2Orig_controlPoints_0__xValue.o" "pCubeShape2Orig.cp[0].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_0__yValue.o" "pCubeShape2Orig.cp[0].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_0__zValue.o" "pCubeShape2Orig.cp[0].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_1__xValue.o" "pCubeShape2Orig.cp[1].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_1__yValue.o" "pCubeShape2Orig.cp[1].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_1__zValue.o" "pCubeShape2Orig.cp[1].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_2__xValue.o" "pCubeShape2Orig.cp[2].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_2__yValue.o" "pCubeShape2Orig.cp[2].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_2__zValue.o" "pCubeShape2Orig.cp[2].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_3__xValue.o" "pCubeShape2Orig.cp[3].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_3__yValue.o" "pCubeShape2Orig.cp[3].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_3__zValue.o" "pCubeShape2Orig.cp[3].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_4__xValue.o" "pCubeShape2Orig.cp[4].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_4__yValue.o" "pCubeShape2Orig.cp[4].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_4__zValue.o" "pCubeShape2Orig.cp[4].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_5__xValue.o" "pCubeShape2Orig.cp[5].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_5__yValue.o" "pCubeShape2Orig.cp[5].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_5__zValue.o" "pCubeShape2Orig.cp[5].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_6__xValue.o" "pCubeShape2Orig.cp[6].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_6__yValue.o" "pCubeShape2Orig.cp[6].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_6__zValue.o" "pCubeShape2Orig.cp[6].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_7__xValue.o" "pCubeShape2Orig.cp[7].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_7__yValue.o" "pCubeShape2Orig.cp[7].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_7__zValue.o" "pCubeShape2Orig.cp[7].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_8__xValue.o" "pCubeShape2Orig.cp[8].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_8__yValue.o" "pCubeShape2Orig.cp[8].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_8__zValue.o" "pCubeShape2Orig.cp[8].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_9__xValue.o" "pCubeShape2Orig.cp[9].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_9__yValue.o" "pCubeShape2Orig.cp[9].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_9__zValue.o" "pCubeShape2Orig.cp[9].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_10__xValue.o" "pCubeShape2Orig.cp[10].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_10__yValue.o" "pCubeShape2Orig.cp[10].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_10__zValue.o" "pCubeShape2Orig.cp[10].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_11__xValue.o" "pCubeShape2Orig.cp[11].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_11__yValue.o" "pCubeShape2Orig.cp[11].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_11__zValue.o" "pCubeShape2Orig.cp[11].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_12__xValue.o" "pCubeShape2Orig.cp[12].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_12__yValue.o" "pCubeShape2Orig.cp[12].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_12__zValue.o" "pCubeShape2Orig.cp[12].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_13__xValue.o" "pCubeShape2Orig.cp[13].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_13__yValue.o" "pCubeShape2Orig.cp[13].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_13__zValue.o" "pCubeShape2Orig.cp[13].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_14__xValue.o" "pCubeShape2Orig.cp[14].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_14__yValue.o" "pCubeShape2Orig.cp[14].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_14__zValue.o" "pCubeShape2Orig.cp[14].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_15__xValue.o" "pCubeShape2Orig.cp[15].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_15__yValue.o" "pCubeShape2Orig.cp[15].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_15__zValue.o" "pCubeShape2Orig.cp[15].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_16__xValue.o" "pCubeShape2Orig.cp[16].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_16__yValue.o" "pCubeShape2Orig.cp[16].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_16__zValue.o" "pCubeShape2Orig.cp[16].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_17__xValue.o" "pCubeShape2Orig.cp[17].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_17__yValue.o" "pCubeShape2Orig.cp[17].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_17__zValue.o" "pCubeShape2Orig.cp[17].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_18__xValue.o" "pCubeShape2Orig.cp[18].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_18__yValue.o" "pCubeShape2Orig.cp[18].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_18__zValue.o" "pCubeShape2Orig.cp[18].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_19__xValue.o" "pCubeShape2Orig.cp[19].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_19__yValue.o" "pCubeShape2Orig.cp[19].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_19__zValue.o" "pCubeShape2Orig.cp[19].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_20__xValue.o" "pCubeShape2Orig.cp[20].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_20__yValue.o" "pCubeShape2Orig.cp[20].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_20__zValue.o" "pCubeShape2Orig.cp[20].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_21__xValue.o" "pCubeShape2Orig.cp[21].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_21__yValue.o" "pCubeShape2Orig.cp[21].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_21__zValue.o" "pCubeShape2Orig.cp[21].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_22__xValue.o" "pCubeShape2Orig.cp[22].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_22__yValue.o" "pCubeShape2Orig.cp[22].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_22__zValue.o" "pCubeShape2Orig.cp[22].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_23__xValue.o" "pCubeShape2Orig.cp[23].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_23__yValue.o" "pCubeShape2Orig.cp[23].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_23__zValue.o" "pCubeShape2Orig.cp[23].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_24__xValue.o" "pCubeShape2Orig.cp[24].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_24__yValue.o" "pCubeShape2Orig.cp[24].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_24__zValue.o" "pCubeShape2Orig.cp[24].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_25__xValue.o" "pCubeShape2Orig.cp[25].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_25__yValue.o" "pCubeShape2Orig.cp[25].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_25__zValue.o" "pCubeShape2Orig.cp[25].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_26__xValue.o" "pCubeShape2Orig.cp[26].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_26__yValue.o" "pCubeShape2Orig.cp[26].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_26__zValue.o" "pCubeShape2Orig.cp[26].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_27__xValue.o" "pCubeShape2Orig.cp[27].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_27__yValue.o" "pCubeShape2Orig.cp[27].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_27__zValue.o" "pCubeShape2Orig.cp[27].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_28__xValue.o" "pCubeShape2Orig.cp[28].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_28__yValue.o" "pCubeShape2Orig.cp[28].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_28__zValue.o" "pCubeShape2Orig.cp[28].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_29__xValue.o" "pCubeShape2Orig.cp[29].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_29__yValue.o" "pCubeShape2Orig.cp[29].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_29__zValue.o" "pCubeShape2Orig.cp[29].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_30__xValue.o" "pCubeShape2Orig.cp[30].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_30__yValue.o" "pCubeShape2Orig.cp[30].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_30__zValue.o" "pCubeShape2Orig.cp[30].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_31__xValue.o" "pCubeShape2Orig.cp[31].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_31__yValue.o" "pCubeShape2Orig.cp[31].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_31__zValue.o" "pCubeShape2Orig.cp[31].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_32__xValue.o" "pCubeShape2Orig.cp[32].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_32__yValue.o" "pCubeShape2Orig.cp[32].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_32__zValue.o" "pCubeShape2Orig.cp[32].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_33__xValue.o" "pCubeShape2Orig.cp[33].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_33__yValue.o" "pCubeShape2Orig.cp[33].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_33__zValue.o" "pCubeShape2Orig.cp[33].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_34__xValue.o" "pCubeShape2Orig.cp[34].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_34__yValue.o" "pCubeShape2Orig.cp[34].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_34__zValue.o" "pCubeShape2Orig.cp[34].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_35__xValue.o" "pCubeShape2Orig.cp[35].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_35__yValue.o" "pCubeShape2Orig.cp[35].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_35__zValue.o" "pCubeShape2Orig.cp[35].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_36__xValue.o" "pCubeShape2Orig.cp[36].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_36__yValue.o" "pCubeShape2Orig.cp[36].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_36__zValue.o" "pCubeShape2Orig.cp[36].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_37__xValue.o" "pCubeShape2Orig.cp[37].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_37__yValue.o" "pCubeShape2Orig.cp[37].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_37__zValue.o" "pCubeShape2Orig.cp[37].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_38__xValue.o" "pCubeShape2Orig.cp[38].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_38__yValue.o" "pCubeShape2Orig.cp[38].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_38__zValue.o" "pCubeShape2Orig.cp[38].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_39__xValue.o" "pCubeShape2Orig.cp[39].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_39__yValue.o" "pCubeShape2Orig.cp[39].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_39__zValue.o" "pCubeShape2Orig.cp[39].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_40__xValue.o" "pCubeShape2Orig.cp[40].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_40__yValue.o" "pCubeShape2Orig.cp[40].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_40__zValue.o" "pCubeShape2Orig.cp[40].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_41__xValue.o" "pCubeShape2Orig.cp[41].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_41__yValue.o" "pCubeShape2Orig.cp[41].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_41__zValue.o" "pCubeShape2Orig.cp[41].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_42__xValue.o" "pCubeShape2Orig.cp[42].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_42__yValue.o" "pCubeShape2Orig.cp[42].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_42__zValue.o" "pCubeShape2Orig.cp[42].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_43__xValue.o" "pCubeShape2Orig.cp[43].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_43__yValue.o" "pCubeShape2Orig.cp[43].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_43__zValue.o" "pCubeShape2Orig.cp[43].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_44__xValue.o" "pCubeShape2Orig.cp[44].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_44__yValue.o" "pCubeShape2Orig.cp[44].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_44__zValue.o" "pCubeShape2Orig.cp[44].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_45__xValue.o" "pCubeShape2Orig.cp[45].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_45__yValue.o" "pCubeShape2Orig.cp[45].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_45__zValue.o" "pCubeShape2Orig.cp[45].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_46__xValue.o" "pCubeShape2Orig.cp[46].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_46__yValue.o" "pCubeShape2Orig.cp[46].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_46__zValue.o" "pCubeShape2Orig.cp[46].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_47__xValue.o" "pCubeShape2Orig.cp[47].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_47__yValue.o" "pCubeShape2Orig.cp[47].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_47__zValue.o" "pCubeShape2Orig.cp[47].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_48__xValue.o" "pCubeShape2Orig.cp[48].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_48__yValue.o" "pCubeShape2Orig.cp[48].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_48__zValue.o" "pCubeShape2Orig.cp[48].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_49__xValue.o" "pCubeShape2Orig.cp[49].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_49__yValue.o" "pCubeShape2Orig.cp[49].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_49__zValue.o" "pCubeShape2Orig.cp[49].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_50__xValue.o" "pCubeShape2Orig.cp[50].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_50__yValue.o" "pCubeShape2Orig.cp[50].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_50__zValue.o" "pCubeShape2Orig.cp[50].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_51__xValue.o" "pCubeShape2Orig.cp[51].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_51__yValue.o" "pCubeShape2Orig.cp[51].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_51__zValue.o" "pCubeShape2Orig.cp[51].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_52__xValue.o" "pCubeShape2Orig.cp[52].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_52__yValue.o" "pCubeShape2Orig.cp[52].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_52__zValue.o" "pCubeShape2Orig.cp[52].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_53__xValue.o" "pCubeShape2Orig.cp[53].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_53__yValue.o" "pCubeShape2Orig.cp[53].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_53__zValue.o" "pCubeShape2Orig.cp[53].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_54__xValue.o" "pCubeShape2Orig.cp[54].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_54__yValue.o" "pCubeShape2Orig.cp[54].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_54__zValue.o" "pCubeShape2Orig.cp[54].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_55__xValue.o" "pCubeShape2Orig.cp[55].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_55__yValue.o" "pCubeShape2Orig.cp[55].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_55__zValue.o" "pCubeShape2Orig.cp[55].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_56__xValue.o" "pCubeShape2Orig.cp[56].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_56__yValue.o" "pCubeShape2Orig.cp[56].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_56__zValue.o" "pCubeShape2Orig.cp[56].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_57__xValue.o" "pCubeShape2Orig.cp[57].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_57__yValue.o" "pCubeShape2Orig.cp[57].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_57__zValue.o" "pCubeShape2Orig.cp[57].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_58__xValue.o" "pCubeShape2Orig.cp[58].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_58__yValue.o" "pCubeShape2Orig.cp[58].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_58__zValue.o" "pCubeShape2Orig.cp[58].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_59__xValue.o" "pCubeShape2Orig.cp[59].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_59__yValue.o" "pCubeShape2Orig.cp[59].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_59__zValue.o" "pCubeShape2Orig.cp[59].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_60__xValue.o" "pCubeShape2Orig.cp[60].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_60__yValue.o" "pCubeShape2Orig.cp[60].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_60__zValue.o" "pCubeShape2Orig.cp[60].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_61__xValue.o" "pCubeShape2Orig.cp[61].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_61__yValue.o" "pCubeShape2Orig.cp[61].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_61__zValue.o" "pCubeShape2Orig.cp[61].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_62__xValue.o" "pCubeShape2Orig.cp[62].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_62__yValue.o" "pCubeShape2Orig.cp[62].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_62__zValue.o" "pCubeShape2Orig.cp[62].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_63__xValue.o" "pCubeShape2Orig.cp[63].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_63__yValue.o" "pCubeShape2Orig.cp[63].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_63__zValue.o" "pCubeShape2Orig.cp[63].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_64__xValue.o" "pCubeShape2Orig.cp[64].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_64__yValue.o" "pCubeShape2Orig.cp[64].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_64__zValue.o" "pCubeShape2Orig.cp[64].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_65__xValue.o" "pCubeShape2Orig.cp[65].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_65__yValue.o" "pCubeShape2Orig.cp[65].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_65__zValue.o" "pCubeShape2Orig.cp[65].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_66__xValue.o" "pCubeShape2Orig.cp[66].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_66__yValue.o" "pCubeShape2Orig.cp[66].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_66__zValue.o" "pCubeShape2Orig.cp[66].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_67__xValue.o" "pCubeShape2Orig.cp[67].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_67__yValue.o" "pCubeShape2Orig.cp[67].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_67__zValue.o" "pCubeShape2Orig.cp[67].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_68__xValue.o" "pCubeShape2Orig.cp[68].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_68__yValue.o" "pCubeShape2Orig.cp[68].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_68__zValue.o" "pCubeShape2Orig.cp[68].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_69__xValue.o" "pCubeShape2Orig.cp[69].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_69__yValue.o" "pCubeShape2Orig.cp[69].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_69__zValue.o" "pCubeShape2Orig.cp[69].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_70__xValue.o" "pCubeShape2Orig.cp[70].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_70__yValue.o" "pCubeShape2Orig.cp[70].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_70__zValue.o" "pCubeShape2Orig.cp[70].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_71__xValue.o" "pCubeShape2Orig.cp[71].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_71__yValue.o" "pCubeShape2Orig.cp[71].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_71__zValue.o" "pCubeShape2Orig.cp[71].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_72__xValue.o" "pCubeShape2Orig.cp[72].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_72__yValue.o" "pCubeShape2Orig.cp[72].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_72__zValue.o" "pCubeShape2Orig.cp[72].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_73__xValue.o" "pCubeShape2Orig.cp[73].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_73__yValue.o" "pCubeShape2Orig.cp[73].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_73__zValue.o" "pCubeShape2Orig.cp[73].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_74__xValue.o" "pCubeShape2Orig.cp[74].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_74__yValue.o" "pCubeShape2Orig.cp[74].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_74__zValue.o" "pCubeShape2Orig.cp[74].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_75__xValue.o" "pCubeShape2Orig.cp[75].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_75__yValue.o" "pCubeShape2Orig.cp[75].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_75__zValue.o" "pCubeShape2Orig.cp[75].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_76__xValue.o" "pCubeShape2Orig.cp[76].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_76__yValue.o" "pCubeShape2Orig.cp[76].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_76__zValue.o" "pCubeShape2Orig.cp[76].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_77__xValue.o" "pCubeShape2Orig.cp[77].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_77__yValue.o" "pCubeShape2Orig.cp[77].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_77__zValue.o" "pCubeShape2Orig.cp[77].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_78__xValue.o" "pCubeShape2Orig.cp[78].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_78__yValue.o" "pCubeShape2Orig.cp[78].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_78__zValue.o" "pCubeShape2Orig.cp[78].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_79__xValue.o" "pCubeShape2Orig.cp[79].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_79__yValue.o" "pCubeShape2Orig.cp[79].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_79__zValue.o" "pCubeShape2Orig.cp[79].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_80__xValue.o" "pCubeShape2Orig.cp[80].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_80__yValue.o" "pCubeShape2Orig.cp[80].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_80__zValue.o" "pCubeShape2Orig.cp[80].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_81__xValue.o" "pCubeShape2Orig.cp[81].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_81__yValue.o" "pCubeShape2Orig.cp[81].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_81__zValue.o" "pCubeShape2Orig.cp[81].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_82__xValue.o" "pCubeShape2Orig.cp[82].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_82__yValue.o" "pCubeShape2Orig.cp[82].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_82__zValue.o" "pCubeShape2Orig.cp[82].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_83__xValue.o" "pCubeShape2Orig.cp[83].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_83__yValue.o" "pCubeShape2Orig.cp[83].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_83__zValue.o" "pCubeShape2Orig.cp[83].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_84__xValue.o" "pCubeShape2Orig.cp[84].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_84__yValue.o" "pCubeShape2Orig.cp[84].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_84__zValue.o" "pCubeShape2Orig.cp[84].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_85__xValue.o" "pCubeShape2Orig.cp[85].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_85__yValue.o" "pCubeShape2Orig.cp[85].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_85__zValue.o" "pCubeShape2Orig.cp[85].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_86__xValue.o" "pCubeShape2Orig.cp[86].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_86__yValue.o" "pCubeShape2Orig.cp[86].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_86__zValue.o" "pCubeShape2Orig.cp[86].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_87__xValue.o" "pCubeShape2Orig.cp[87].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_87__yValue.o" "pCubeShape2Orig.cp[87].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_87__zValue.o" "pCubeShape2Orig.cp[87].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_88__xValue.o" "pCubeShape2Orig.cp[88].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_88__yValue.o" "pCubeShape2Orig.cp[88].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_88__zValue.o" "pCubeShape2Orig.cp[88].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_89__xValue.o" "pCubeShape2Orig.cp[89].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_89__yValue.o" "pCubeShape2Orig.cp[89].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_89__zValue.o" "pCubeShape2Orig.cp[89].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_90__xValue.o" "pCubeShape2Orig.cp[90].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_90__yValue.o" "pCubeShape2Orig.cp[90].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_90__zValue.o" "pCubeShape2Orig.cp[90].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_91__xValue.o" "pCubeShape2Orig.cp[91].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_91__yValue.o" "pCubeShape2Orig.cp[91].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_91__zValue.o" "pCubeShape2Orig.cp[91].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_92__xValue.o" "pCubeShape2Orig.cp[92].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_92__yValue.o" "pCubeShape2Orig.cp[92].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_92__zValue.o" "pCubeShape2Orig.cp[92].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_93__xValue.o" "pCubeShape2Orig.cp[93].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_93__yValue.o" "pCubeShape2Orig.cp[93].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_93__zValue.o" "pCubeShape2Orig.cp[93].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_94__xValue.o" "pCubeShape2Orig.cp[94].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_94__yValue.o" "pCubeShape2Orig.cp[94].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_94__zValue.o" "pCubeShape2Orig.cp[94].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_95__xValue.o" "pCubeShape2Orig.cp[95].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_95__yValue.o" "pCubeShape2Orig.cp[95].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_95__zValue.o" "pCubeShape2Orig.cp[95].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_96__xValue.o" "pCubeShape2Orig.cp[96].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_96__yValue.o" "pCubeShape2Orig.cp[96].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_96__zValue.o" "pCubeShape2Orig.cp[96].zv"
		;
connectAttr "pCubeShape2Orig_controlPoints_97__xValue.o" "pCubeShape2Orig.cp[97].xv"
		;
connectAttr "pCubeShape2Orig_controlPoints_97__yValue.o" "pCubeShape2Orig.cp[97].yv"
		;
connectAttr "pCubeShape2Orig_controlPoints_97__zValue.o" "pCubeShape2Orig.cp[97].zv"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn4SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "blendShape1.mlpr" "shapeEditorManager.bspr[0]";
connectAttr "blinn1.oc" "blinn1SG.ss";
connectAttr "groupId8.msg" "blinn1SG.gn" -na;
connectAttr "pCubeShape2.iog.og[1]" "blinn1SG.dsm" -na;
connectAttr "blinn1SG.msg" "materialInfo1.sg";
connectAttr "blinn1.msg" "materialInfo1.m";
connectAttr ":defaultColorMgtGlobals.cme" "file1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file1.ws";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "blinn2.oc" "blinn2SG.ss";
connectAttr "groupId9.msg" "blinn2SG.gn" -na;
connectAttr "pCubeShape2.iog.og[2]" "blinn2SG.dsm" -na;
connectAttr "blinn2SG.msg" "materialInfo2.sg";
connectAttr "blinn2.msg" "materialInfo2.m";
connectAttr "blinn3.oc" "blinn3SG.ss";
connectAttr "groupId10.msg" "blinn3SG.gn" -na;
connectAttr "pCubeShape2.iog.og[3]" "blinn3SG.dsm" -na;
connectAttr "blinn3SG.msg" "materialInfo3.sg";
connectAttr "blinn3.msg" "materialInfo3.m";
connectAttr "blinn4.oc" "blinn4SG.ss";
connectAttr "groupId11.msg" "blinn4SG.gn" -na;
connectAttr "pCubeShape2.iog.og[4]" "blinn4SG.dsm" -na;
connectAttr "blinn4SG.msg" "materialInfo4.sg";
connectAttr "blinn4.msg" "materialInfo4.m";
connectAttr "blendShape1GroupParts.og" "blendShape1.ip[0].ig";
connectAttr "blendShape1GroupId.id" "blendShape1.ip[0].gi";
connectAttr "shapeEditorManager.obsv[0]" "blendShape1.tgdt[0].dpvs";
connectAttr "blendShape1_weirdo.o" "blendShape1.w[0]";
connectAttr "pCubeShape2Orig.w" "groupParts6.ig";
connectAttr "groupId7.id" "groupParts6.gi";
connectAttr "groupParts6.og" "groupParts7.ig";
connectAttr "groupId8.id" "groupParts7.gi";
connectAttr "groupParts7.og" "groupParts8.ig";
connectAttr "groupId9.id" "groupParts8.gi";
connectAttr "groupParts8.og" "groupParts9.ig";
connectAttr "groupId10.id" "groupParts9.gi";
connectAttr "groupParts9.og" "groupParts10.ig";
connectAttr "groupId11.id" "groupParts10.gi";
connectAttr "groupParts12.og" "tweak1.ip[0].ig";
connectAttr "groupId13.id" "tweak1.ip[0].gi";
connectAttr "blendShape1GroupId.msg" "blendShape1Set.gn" -na;
connectAttr "pCubeShape2.iog.og[5]" "blendShape1Set.dsm" -na;
connectAttr "blendShape1.msg" "blendShape1Set.ub[0]";
connectAttr "tweak1.og[0]" "blendShape1GroupParts.ig";
connectAttr "blendShape1GroupId.id" "blendShape1GroupParts.gi";
connectAttr "groupId13.msg" "tweakSet1.gn" -na;
connectAttr "pCubeShape2.iog.og[6]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "groupParts10.og" "groupParts12.ig";
connectAttr "groupId13.id" "groupParts12.gi";
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "blinn2SG.pa" ":renderPartition.st" -na;
connectAttr "blinn3SG.pa" ":renderPartition.st" -na;
connectAttr "blinn4SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn2.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn3.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn4.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "pCubeShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
// End of cube-blend.ma
