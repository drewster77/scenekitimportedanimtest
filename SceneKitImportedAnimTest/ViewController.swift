//
//  ViewController.swift
//  SceneKitImportedAnimTest
//
//  Created by Andrew Benson on 4/9/18.
//  Copyright © 2018 Nuclear Cyborg Corp. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {
    private struct AnimDetails {
        let fileName : String
        let modelName: String
    }
    
    private var anims: [AnimDetails] = [
        AnimDetails(fileName: "anim.scnassets/heart-rotate.dae", modelName: "pPlane3"),
        AnimDetails(fileName: "anim.scnassets/heart-translate.dae", modelName: "pPlane3"),
        AnimDetails(fileName: "anim.scnassets/heart-rotate+translate.dae", modelName: "pPlane3"),
        AnimDetails(fileName: "anim.scnassets/cube-blend.dae", modelName: "pCube2")
    ]
    @IBOutlet weak var sceneView: SCNView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    
    @IBAction func segmentControlValueChanged(_ sender: Any) {
        activateModelWithIndex(segmentControl.selectedSegmentIndex)
    }
    
    private func activateModelWithIndex(_ index: Int) {
        activeNode?.removeFromParentNode()
        
        let anim = anims[segmentControl.selectedSegmentIndex]
        guard let nodeScene = SCNScene(named: anim.fileName) else {
            fatalError("COULD NOT OPEN SCENE FILE \(anim.fileName)")
        }
        guard let node = nodeScene.rootNode.childNode(withName: anim.modelName, recursively: true) else {
            fatalError("COULD NOT FIND NODE \(anim.modelName) in file \(anim.fileName)")
        }
        activeNode = node
        
        print("\nActivated node \(anim.modelName) from file \(anim.fileName)")
        for key in node.animationKeys {
            print("    - Animation key \(key)")
            if let p = node.animationPlayer(forKey: key) {
                print("          Player details: \(p)")
                print("           blendFactor \(p.blendFactor)")
                print("           Animation details: \(p.animation)")

                p.animation.animationDidStart = { (anim, bla) in
                    print("             ** anim did START.")
                }
                
                p.animation.animationDidStop = { (anim, bla, foo) in
                    print("             ** anim did STOP.")
                }
            }
        }
        print("")
        scene.rootNode.addChildNode(node)
    }
    
    let scene = SCNScene()
    let camNode = SCNNode()
    var activeNode : SCNNode?
    

   
    // MARK: - VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // FLOOR
        let floorNode = SCNNode()
        floorNode.geometry = SCNBox(width: 100, height: 1, length: 100, chamferRadius: 5)
        floorNode.position = SCNVector3Make(0, -2, 0)
        let mat = SCNMaterial()
        mat.diffuse.contents = UIColor(red: 0.20, green: 0.40, blue: 0.80, alpha: 0.90)
        mat.specular.contents = UIColor.white
        mat.shininess = 1000.0
        
        floorNode.geometry!.materials = [mat]
        scene.rootNode.addChildNode(floorNode)
        
        
        // LIGHTS
        let omniLightNode = SCNNode()
        let light = SCNLight()
        light.type = .omni
        light.intensity = 500
        omniLightNode.light = light
        scene.rootNode.addChildNode(omniLightNode)
        
        let ambientLightNode = SCNNode()
        let light2 = SCNLight()
        light2.type = .ambient
        light2.intensity = 500
        light2.temperature = 5000
        ambientLightNode.light = light2
        scene.rootNode.addChildNode(ambientLightNode)
        
        let spotLightNode = SCNNode()
        let light3 = SCNLight()
        light3.type = .spot
        light3.intensity = 1000
        spotLightNode.light = light3
        spotLightNode.position = SCNVector3(0, 1, 1)
        spotLightNode.look(at: SCNVector3(0, 0, 0))
        scene.rootNode.addChildNode(spotLightNode)
        
        
        // CAMERA
        camNode.position = SCNVector3Make(0, 0, 0.06)
        camNode.camera = SCNCamera()
        camNode.camera?.zNear = 0.01
        camNode.camera?.zFar = 1000
        camNode.look(at: SCNVector3(0, 0, 0), up: SCNVector3(0, 1, 0), localFront: SCNVector3(0, 0, -1))
        scene.rootNode.addChildNode(camNode)
        
        sceneView.scene = scene
        sceneView.pointOfView = camNode
        sceneView.autoenablesDefaultLighting = false
        sceneView.allowsCameraControl = false

        activateModelWithIndex(0)
    }
}

